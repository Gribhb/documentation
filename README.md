# R2Devops documentation

## Repository

This repo is about:

* Documentation of r2devops.io

```
.
├── docs            # Documentation sources
├── mkdocs.yml      # Documentation configuration
├── Pipfile         # Pipenv dependency file to build doc
└── Pipfile.lock
```

Documentation is built using [Mkdocs](https://www.mkdocs.org) and [Material for
Mkdocs](https://squidfunk.github.io/mkdocs-material/){:target="_blank"}.

## How to update doc

### Option 1: with Docker

1. Run the server
    ```shell
    docker run --rm -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material:latest
    ```
1. See your update in live: [https://localhost:8000](https://localhost:8000)

### Option 2: with Python

1. Be sure to have requirements
    * `python3`
    * `pipenv`
1. Install dependencies
    ```shell
    pipenv install
    ```
1. Launch Mkdocs with hot-reload:
    ```shell
    pipenv run mkdocs serve
    ```
1. See your update in live: [https://localhost:8000](https://localhost:8000)
