---
title: 1.17.0 release
description: > 
    R2Devops 1.17.0 brings enhanced dashboard analysis, restoration of compliancy tab, documentation about hardcoded jobs, and support for CA certificates for both frontend and backend services.

date: 2023-09-15
---

# **R2Devops 1.17.0 release**

!!! info "Docker images versions"
    - Backend: `v1.18.0`
    - Frontend: `v1.13.1` 

!!! info "Chart helm version"
    `v0.5.0`

## **📊 Enhanced Dashboard Analysis**

- We are thrilled to announce the restoration of compliancy in the dashboard analysis page. Now, you can enjoy a seamless experience with our new paginated table, equipped with sorting functions.

![Dashboard Analysis](../../images/compliancy.gif)

## **📖 Improved Documentation for Maintainability**

- Inside the maintainability table, we've added documentation for hardcoded jobs. This documentation provides insights into addressing this common issue, empowering you to improve the maintainability of your CI/CD pipelines.

![Maintainability Documentation](../../images/hardcoded-doc.gif)

## **🔒 Support for custom CA Certificates**

- R2DevOps now supports custom CA certificates, on **self-hosted instances**. It enhances security and simplify connections to private domains on self-hosted services.


### Minor Updates

- Allow to manually search labels in the marketplace
- Fix broken link `/dashboard`
- Live refresh on the analysis dashboard won't be selected by default anymore
