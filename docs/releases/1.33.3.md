---
title: "1.33.3 Release"
description: >
    This release introduces improvements to template usage and visibility, enhances documentation, and refines minor functionalities.
date: 2024-03-05
---

# R2Devops 1.33.3 Release

!!! info "Docker Image Versions"
    - Backend: `v1.31.5`
    - Frontend: `v1.29.3`
    - Helm chart: `v1.33.3`


## ✨ Include templates more efficiently with project include

**[Project include](https://docs.gitlab.com/ee/ci/yaml/#includeproject)** is
now the officially supported way to include R2Devops templates, offering:

- Support of local nested templates
- R2Devops is not anymore a dependency to run your pipelines
- No template token required to use private template (uses GitLab
  authorizations)
- Reduced latency to launch pipelines
- No more network configuration required for GitLab instance to reach R2Devops
  instance

**Examples:**

- Previous `include:remote`:
    ```yaml
    include:
      - remote: 'https://api.r2devops.io/job/r/gitlab/r2devops/hub/docker_build@2.0.1.yaml'
    ```
- New `include:project`:
    ```yaml
    include:
      - project: 'r2devops/hub'
        ref: 'docker_build@2.0.1'
        file: 'jobs/docker_build/docker_build.yml'
    ```

!!! success "**No breaking changes for existing `include:remote` usages**"
    Existing pipelines using `include:remote` will continue to work as intended


## 👁️ Visibility and documentation improvements

- **Template visibility:** The visibility of a template is now the same as
  the visibility of its GitLab repository
- **Improved documentation:** The documentation for templates has been revised
  for better clarity and user experience

-----

<div>
<a alt="Open a ticket" style="width:100%; display:flex; justify-content:center" href="https://r2devops.io">
    <button class="md-button border-radius-10" style="width:100%; display:flex; justify-content:center">
        Try it
    </button>
</a>
</div>
