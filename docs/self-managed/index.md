# Self-managed

This section describes how to setup your self-managed instance of R2Devops.
To find out more about the application architecture, [read this](/self-managed/architecture/).

Several methods are available:

- [⏱️ Trying R2Devops on your local computer using Docker-compose](/self-managed/local-docker-compose/)
- [🐳 Running R2Devops using Docker-compose](/self-managed/docker-compose/)
- [🚀 Running R2Devops on Kubernetes using a Helm chart](/self-managed/kubernetes/)
